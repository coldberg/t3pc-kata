#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stack>
#include <memory>
#include <regex>
#include <iterator>
#include <unordered_map>
#include <list>
#include <tuple>

struct Simulator {
    enum opcode_type { IM, AR, SW, PU, PO, AD, SU, MU, DI };

    typedef std::int64_t register_type;

    struct instruction_type:
        std::pair<opcode_type, register_type> 
    {
        instruction_type (opcode_type s_op, register_type s_val = register_type ())
        :   std::pair<opcode_type, register_type> (s_op, s_val) 
        {}
    };

    typedef std::vector<instruction_type> instruction_list_type;

    static auto simulate (const instruction_list_type& s_asm,
        const std::vector<register_type>& s_args) 
    {
        auto r0 = register_type ();
        auto r1 = register_type ();
        std::stack<register_type> s_stack;

        for (const auto& s_instr : s_asm) {            
            switch (s_instr.first) {
                case IM : r0 = s_instr.second; break;
                case AR : r0 = s_args.at (s_instr.second); break;
                case SW : std::swap (r1, r0); break;
                case PU : s_stack.push (r0); break;
                case PO : r0 = s_stack.top (); s_stack.pop (); break;
                case AD : r0 += r1; break;
                case SU : r0 -= r1; break;
                case MU : r0 *= r1; break;
                case DI : r0 /= r1; break;
                default: throw std::logic_error ("Bad opcode : " + std::to_string (s_instr.second));
            }
        }
        return r0;
    }

};

struct Node {
    typedef Simulator::register_type register_type;

	enum operation : int {
		imm = '\'',
		arg = '$',
		add = '+',
		sub = '-',
		mul = '*',
		div = '/'
	};

	typedef std::shared_ptr<Node> pointer_type;
	
	int op () const { return m_operation ; }
	virtual pointer_type a () const { return nullptr; }
	virtual pointer_type b () const { return nullptr; }
	virtual register_type n () const { return register_type (); }
	
	virtual ~Node () = default;

	Node (int s_operation):
		m_operation (s_operation)
	{}
	
	virtual std::string stringify () const {
		return "";
	}

private:
	int m_operation = imm;
};

template <typename _Node>
struct NodeNew {
	template <typename... _Args>
	static auto New (_Args&&... args) {
		return Node::pointer_type (std::make_shared<_Node>(std::forward<_Args>(args)...));
	}
};

struct Unary: Node, NodeNew<Unary> {
	Unary (int s_operation, register_type s_imm_or_arg):
		Node (s_operation),
		m_imm_or_arg (s_imm_or_arg)
	{}
	
	register_type n () const override { return m_imm_or_arg; }
	
	std::string stringify () const override {
		return char (op ()) + std::to_string(n ());
	}
	
private:
	register_type m_imm_or_arg;
};

struct Binary: Node, NodeNew<Binary> {
	Binary (int s_operation, pointer_type s_left, pointer_type s_right):
		Node (s_operation),
		m_left (s_left),
		m_right (s_right)
	{}
	
	pointer_type a () const override { return m_left; }
	pointer_type b () const override { return m_right; }
	
	std::string stringify () const override {
		return std::string ("(") + char (op ()) + " " + a ()->stringify() + " " + b ()->stringify() + ")";
	}
	
private:
	pointer_type m_left, m_right;
};

struct Compiler {   
    typedef Simulator::register_type register_type;

    static register_type to_register (const std::string& v) {
        return std::stoll (v);
    }

    static auto tokenize (const std::string& s_input) {
        static const std::regex st_token_regex (R"(\s*([-+*/\(\)\[\]]|[A-Za-z]+|[0-9]+)\s*)", std::regex::optimize);
        std::list<std::string> s_token_list;

        std::sregex_iterator s_tokbeg (s_input.begin (), s_input.end (), st_token_regex), s_tokend;
        std::transform (s_tokbeg, s_tokend, 
            std::back_inserter (s_token_list), 
            [] (const auto& s_tok) {
                return s_tok [1].str ();
            }
        );

        return s_token_list;
    }

    /* Shitty emulation of javascript types */

    struct Object {
        typedef std::shared_ptr<Object> pointer_type;
        virtual ~Object () = default;
        virtual bool is_list () const { return false; }
        virtual pointer_type shift () { throw std::logic_error ("this is not a list"); }
        virtual bool equals (const std::string& s_value) const { return false; }
        virtual std::string value () const { return ""; }
    };

    struct List: Object {
        typedef std::list<std::shared_ptr<Object>> value_type;        
        List (value_type s_value): m_value (std::move (s_value)) {}

        bool is_list () const override { return true; }
        
        static auto New (value_type s_value) {
            return pointer_type{std::make_shared<List> (std::move (s_value))};
        }

        pointer_type shift () override { 
            auto it = std::move (m_value.front ());
            m_value.pop_front ();
            return it;
        }

        value_type m_value;
    };

    struct String: Object {
        typedef std::string value_type;
        String (value_type s_value): m_value (std::move (s_value)) {}
       
        bool equals (const std::string& s_value) const override { return m_value == s_value; }
        std::string value () const override { return m_value; }

        static auto New (value_type s_value) {
            return pointer_type{std::make_shared<String> (std::move (s_value))};
        }        

        value_type m_value;
    };

    typedef std::list<std::shared_ptr<Object>> list_type;
    typedef std::unordered_map<std::string, int> arg_lookup_type;

    static auto shift (list_type& s_list) 
        -> list_type::value_type
    {
        auto s_tmp = std::move (s_list.front ());
        s_list.pop_front ();
        return s_tmp;
    };
    
    static auto reduce (list_type& s_exp) 
        -> list_type::value_type 
    {
        if (s_exp.size () == 1) {
            return shift (s_exp);
        }
        auto s_collect = list_type{};
        while (s_exp.size ()) {
            auto s_tok = shift (s_exp);
            if (s_collect.size () < 3) {
                s_collect.push_back (s_tok);
                continue;
            }
            s_collect = list_type{List::New (s_collect), s_tok};
        }
        return List::New (s_collect);
    };

    static auto expression (list_type& s_exp) 
        -> list_type::value_type 
    {
        auto s_primary = list_type {};
        auto s_secondary = list_type {};
        while (s_exp.size ()) {
            auto s_tok = shift (s_exp);
            if (!s_tok->equals ("+") && !s_tok->equals ("-")) {
                s_secondary.push_back (s_tok);
                continue;
            }
            s_primary.push_back (reduce (s_secondary));
            s_primary.push_back (s_tok);
            s_secondary = list_type{};
        }
        s_primary.push_back (reduce (s_secondary));
        return reduce (s_primary);
    };
    
    static auto factor (list_type& s_exp) 
        -> list_type::value_type 
    {
        auto s_collect = list_type{};
        while (s_exp.size ()) {
            auto s_tok = shift (s_exp);
            if (s_tok->equals ("(")) {
                s_collect.push_back (factor (s_exp));
                continue;
            } else if (s_tok->equals (")")) {
                return expression (s_collect);
            }
            s_collect.push_back (s_tok);
        }
        return expression (s_collect);
    };
    
	static auto wrap (list_type::value_type s_ast, const arg_lookup_type& s_lut) 
        -> Node::pointer_type 
    {
        if (s_ast->is_list ()) {
            auto a = s_ast->shift ();
            auto o = s_ast->shift ();
            auto b = s_ast->shift ();            
			return Binary::New (o->value ()[0], wrap (a, s_lut), wrap (b, s_lut));
        }

        const auto& s_value = s_ast->value ();
        if (isdigit (s_value [0])) return Unary::New (Node::imm, to_register (s_value));        
        if (isalpha (s_value [0])) return Unary::New (Node::arg, s_lut.at (s_value));        
        throw std::runtime_error ("This is not supposed to happen...");
    }

    static auto pass1 (const std::string& s_program) 
        -> Node::pointer_type
    {
        arg_lookup_type s_args_lookup;
        list_type s_node_list;

        bool s_argument_pass = false;
        int s_argument_index = 0;

        for (const auto& s_tok : tokenize (s_program)) {
            if (s_argument_pass) {
                if (s_tok == "]") { s_argument_pass = false; continue; }                
                s_args_lookup [s_tok] = s_argument_index++;
                continue;
            }
            if (s_tok == "[") { s_argument_pass = true; continue; }
            s_node_list.push_back (String::New (s_tok));
        }      
        return wrap (factor (s_node_list), s_args_lookup);
    }

    static auto pass2 (Node::pointer_type s_ast) 
        -> Node::pointer_type 
    {
        if (s_ast->op () == Node::imm || s_ast->op () == Node::arg) {
            return s_ast;
        }

        auto a = pass2 (s_ast->a ());
        auto b = pass2 (s_ast->b ());
        if (a->op () != Node::imm || b->op () != Node::imm) {
            return Binary::New (s_ast->op (), a, b);
        }

        auto s_result = register_type ();
        switch (s_ast->op ()) {
            case Node::add: s_result = a->n () + b->n (); break;
            case Node::sub: s_result = a->n () - b->n (); break;
            case Node::mul: s_result = a->n () * b->n (); break;
            case Node::div: s_result = a->n () / b->n (); break;
        }

        return Unary::New (Node::imm, s_result);
    }

    static void generate (Simulator::instruction_list_type& s_output, 
                          Node::pointer_type s_ast) 
    {        
        if (s_ast->op () == Node::imm || s_ast->op () == Node::arg) {
            if (s_ast->op () == Node::imm) {
                s_output.emplace_back (Simulator::IM, s_ast->n ());
            }
            if (s_ast->op () == Node::arg) {
                s_output.emplace_back (Simulator::AR, s_ast->n ());
            }
            s_output.emplace_back (Simulator::PU);
            return;
        }

        generate (s_output, s_ast->a ());
        generate (s_output, s_ast->b ());

        s_output.emplace_back (Simulator::PO);
        s_output.emplace_back (Simulator::SW);
        s_output.emplace_back (Simulator::PO);

        switch (s_ast->op ()) {
            case Node::add: s_output.emplace_back (Simulator::AD); break;
            case Node::sub: s_output.emplace_back (Simulator::SU); break;
            case Node::mul: s_output.emplace_back (Simulator::MU); break;
            case Node::div: s_output.emplace_back (Simulator::DI); break;
        }

        s_output.emplace_back (Simulator::PU);
    }

    static auto pass3 (Node::pointer_type s_ast) 
        -> Simulator::instruction_list_type 
    {
        auto s_output = Simulator::instruction_list_type{};
        generate (s_output, s_ast);
        return s_output;
    }

    
};



int main (int argc, const char * argv []) try {

    auto p1 = Compiler::pass1 ("[ x y z ] (2*3*y + 5*y*x - 4*4 + 4*10*x*x + 3*z) / (5*(4 + y) + 3 + 2*2)");
    auto p2 = Compiler::pass2 (p1);
    auto p3 = Compiler::pass3 (p2);

    auto s1 = p1->stringify ();
    auto s2 = p2->stringify ();
    
    std::cout << "\""<< s1 << "\"\n";
    std::cout << "\""<< s2 << "\"\n";

    std::cout << Simulator::simulate (p3, {4, 0, 0}) << "\n";
    std::cout << Simulator::simulate (p3, {4, 8, 0}) << "\n";
    std::cout << Simulator::simulate (p3, {4, 8, 16}) << "\n";
    std::cout << Simulator::simulate (p3, {4, 8, 3}) << "\n";

    return 0;
}
catch (const std::exception& ex) {
    std::cout << ex.what () << "\n";
    return -1;
}
